import React from 'react';
import { BrowserRouter, Route, Link, useLocation } from 'react-router-dom';
import { Menu, Layout, Avatar, Divider } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import './App.css';

import Home from './Home';
import Project1 from './Project1/Project1';
import Project2 from './Project2/Project2';
import Project3 from './Project3/Project3';
import Project4 from './Project4/Project4';
import Project5 from './Project5/Project5';
import Project6 from './Project6/Project6';

import mainRoutes from './mainRoutes';

const { Content, Sider } = Layout;

const SideMenu = () => {
    const location = useLocation();
    const path = location.pathname.split("/")[1];
    const selectedKey = mainRoutes.map(r => r.link).indexOf(path);
    return (
        <Menu defaultSelectedKeys={[selectedKey.toString()]} className="no-border">
            {
                mainRoutes.map((route, i) => (
                    <Menu.Item key={i.toString()}>
                        {route.icon}
                        <span>{route.title}</span>
                        <Link to={route.link}/>
                    </Menu.Item>
                ))
            }
        </Menu>
    );
};

const RouteBuilder = () => (
    <>
        <Route path="/" exact component={Home} />
        <Route path="/project1" exact component={Project1} />
        <Route path="/project2" exact component={Project2} />
        <Route path="/project3" exact component={Project3} />
        <Route path="/project4" exact component={Project4} />
        <Route path="/project5" exact component={Project5} />
        <Route path="/project6" exact component={Project6} />
    </>
);

export default () => {
    return (
        <BrowserRouter>
            <Layout>
                <Sider className="main-sider">
                    <Avatar size={128} icon={<UserOutlined />} className="main-sider-avatar"/>
                    <Divider />
                    <SideMenu />
                </Sider>
                <Layout className="main-container">
                    <Content>
                        <RouteBuilder />
                    </Content>
                </Layout>
            </Layout>
        </BrowserRouter>
    );
};
