import React from 'react';
import { HomeOutlined, FileOutlined } from '@ant-design/icons';

export default [
    {
        "title": "Home",
        "link": "",
        "icon": <HomeOutlined />
    },
    {
        "title": "Project 1",
        "link": "project1",
        "icon": <FileOutlined />
    },
    {
        "title": "Project 2",
        "link": "project2",
        "icon": <FileOutlined />
    },
    {
        "title": "Project 3",
        "link": "project3",
        "icon": <FileOutlined />
    },
    {
        "title": "Project 4",
        "link": "project4",
        "icon": <FileOutlined />
    },
    {
        "title": "Project 5",
        "link": "project5",
        "icon": <FileOutlined />
    },
    {
        "title": "Project 6",
        "link": "project6",
        "icon": <FileOutlined />
    }
];