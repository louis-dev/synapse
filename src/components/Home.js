import React from 'react';
import { PageHeader } from 'antd';

const Home = () => <PageHeader ghost={false} title="Home" />;

export default Home;